#! /bin/bash

set -ex

~/repos/cython/bin/cython_freeze a.py b.py -o main3.c
cython -3 a.py
cython -3 b.py

CFLAGS="-g -I${CONDA_PREFIX}/include/python3.7m/ -L${CONDA_PREFIX}/lib -Wl,-rpath=${CONDA_PREFIX}/lib"
LDFLAGS="-lpython3.7m"

# Static linking
CFLAGS="${CFLAGS} -static -fno-lto"
LDFLAGS="${LDFLAGS} -lm -ldl -pthread -lutil"
gcc $CFLAGS a.c b.c main3.c -o a ${LDFLAGS}

# Create standard library
mkdir -p dist/lib
touch dist/lib/site.py
L=${CONDA_PREFIX}/lib/python3.7
cp -r $L/encodings/ dist/lib/
cp $L/codecs.py dist/lib/
cp $L/io.py dist/lib/
cp $L/abc.py dist/lib/
cp a dist/

# Test
cd dist
PYTHONHOME=`pwd`/lib PYTHONPATH=`pwd`/lib ./a
