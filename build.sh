#! /bin/bash

set -ex

cython -3 a.py
cython -3 b.py
cython -3 amain.pyx

CFLAGS="-g -I${CONDA_PREFIX}/include/python3.7m/ -L${CONDA_PREFIX}/lib -Wl,-rpath=${CONDA_PREFIX}/lib"
LDFLAGS="-lpython3.7m"

# Static linking
CFLAGS="${CFLAGS} -static -fno-lto"
LDFLAGS="${LDFLAGS} -lm -ldl -pthread -lutil"
gcc $CFLAGS a.c b.c amain.c main.c -o a ${LDFLAGS}
