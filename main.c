#include <Python.h>
#include "amain.h"

PyObject *PyInit_a(void);
PyObject *PyInit_b(void);

typedef PyObject *(*pyinit_func)();

int python_init_module(pyinit_func f, char *mod_name)
{
    PyObject *mod = f();
    PyModuleDef *mdef = (PyModuleDef*)mod;
    PyObject *modname = PyUnicode_FromString(mod_name);
    if (!modname) return -1;
    mod = PyModule_NewObject(modname);
    Py_DECREF(modname);
    if (!mod) return -1;
    PyModule_ExecDef(mod, mdef);
    return 0;
}

int main() {
    Py_Initialize();
    if (python_init_module(PyInit_b, "b")) return -1;
    if (python_init_module(PyInit_a, "a")) return -1;
    if (python_init_module(PyInit_amain, "amain")) return -1;
    calla();
    if (PyErr_Occurred())
    {
        PyErr_Print();
        return -1;
    }
    Py_Finalize();
    return 0;
}
